import { combineReducers } from "redux";

import formReducer from "./formReducer";

import {reducer as form} from 'redux-form'

export default combineReducers({
  formReducer,
    form
});
