import * as React from "react";
import store from './store';
import { Provider } from 'react-redux'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Layout } from './components/common/Layout'
import Web3Provider,{ Connectors } from 'web3-react'
import Home from './components/Home'
import HomeNew from './components/home_new'
import LandingHome from './components/Home'
import Login from './components/login'
import Signup from './components/signup'
import GetData from './components/GetData'
import Web3 from 'web3'

import './css/style.css';

const accesstoken =  localStorage.getItem('accesstoken')

const { InjectedConnector, NetworkOnlyConnector } = Connectors
 
const MetaMask = new InjectedConnector({ supportedNetworks: [1, 4] })
 
const Infura = new NetworkOnlyConnector({
  providerURL: 'https://mainnet.infura.io/v3/...'
})
 
const connectors = { MetaMask }

const WithLoginHeader = (props: any) => {
  return(
    <Layout type="loggedin">
      {props.children}
    </Layout>
  )
}
const WithHeader = (props: any) => {
  return(
    <Layout type="landing">
      {props.children}
    </Layout>
  )
}
const WithoutHeader = (props: any) => {
  return(
    <Layout type="guest">
      {props.children}
    </Layout>
  )
}


const App = () => {
  return (
    <Web3Provider
    connectors={connectors}
    libraryName={'web3.js'}
    web3Api={Web3}
  >
  <Provider store={store}>
    <Router>
    <Switch>
    { accesstoken ? (
     <Route exact={true} path="/" component={() => <WithLoginHeader>{<Home/>}</WithLoginHeader>} />):(  <Route exact={true} path="/" component={() => <WithHeader>{<LandingHome/>}</WithHeader>} /> )}
    <Route exact={true} path="/login" component={() => <WithoutHeader>{<Login/>}</WithoutHeader>} />
       <Route exact={true} path="/signup" component={() => <WithoutHeader>{<Signup/>}</WithoutHeader>} />
       <Route exact={true} path="/GetData" component={() => <WithHeader>{<GetData/>}</WithHeader>} />
       
       <Route exact={true} path="/home" component={() => <WithHeader>{<HomeNew/>}</WithHeader>} />
    </Switch>
   </Router>
   </Provider>
   </Web3Provider>
  );
};

export default App;
