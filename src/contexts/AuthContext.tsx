import  { createContext, useState, useEffect } from 'react';
// import {config} from '../config/config'

export const AuthContext = createContext({});

const AuthContextProvider = (props: any) => {
        const [userInfo, setUserInfo] = useState([]);
        const [keydata, setkeydata] = useState('');
        const [publicProfile, setPublicProfile] = useState('');
       
        useEffect(() => {
        //         const accesstoken =  localStorage.getItem('accesstoken')
        //         const userId = localStorage.getItem('userId');
        //      if(accesstoken){
        //       userService.userdetail(userId).then((response) => {
        //         setPublicProfile(response.data.data.profileVisibility)
        //         setUserInfo(response.data.data.user)
        //         setkeydata(response.data.length)
        //          }).catch((e) => {
        //                window.location.href = `${config.appurl}/login`; 
        //                 localStorage.removeItem('accesstoken');
        //                 setUserInfo([])
        //         })
        // //       }
        //       }
              
        },[])
     
        return (
                <AuthContext.Provider key = {keydata} value={{...userInfo,publicProfile}}>
                        {props.children}
                </AuthContext.Provider>
        )
}

export default AuthContextProvider;