export function authHeader() {
 
    // return authorization header with basic auth credentials
    let user = localStorage.getItem('accesstoken')

    if (user && user) {
        return { 'Authorization': 'Bearer ' + user,appVersion:0,deviceType:"web",crossDomain: true};
    } else {
        return {appVersion:0,deviceType:"web", 'Content-Type': 'application/json',crossDomain: true};
    }
}

