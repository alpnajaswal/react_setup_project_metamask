
import Header from './Header';
import Footer from './Footer';
import FooterLogin from './FooterLogin';
import AuthContextProvider from '../../contexts/AuthContext';
import LoginHeader from './LoginHeader';


import { Redirect } from 'react-router-dom';

export const Layout = (props: any) => {
  const accesstoken =  localStorage.getItem('accesstoken')

   if(props.type === 'loggedin' || props.type === 'social') {
    if(accesstoken===null || accesstoken===undefined){
       
		     return <Redirect to='/login' />
		   }
      return (
        <AuthContextProvider>
          <div className="main">
              <LoginHeader/>
              {props.children}
              <Footer/>
          </div>
        </AuthContextProvider>
      )
    }else if(props.type === 'landing') {
      return ( <AuthContextProvider><div className="main">
      <Header/>
       {props.children}
       <Footer/>
   </div></AuthContextProvider>
      )
     

    }else {
      return (
        <div className="main">
        
              {props.children}
              <FooterLogin/>
          </div>

      )
    }
}



