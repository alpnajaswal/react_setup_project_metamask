import React, { useEffect,useState} from 'react'
import { useWeb3Context,Web3Consumer } from 'web3-react'

import { ContractAbi } from './Abi';
import { createAlchemyWeb3 } from "@alch/alchemy-web3";



// This component must be a child of <App> to have access to the appropriate context
export default function MyComponent () {
  // const [account,setaccount] = useState() as any;
  const context = useWeb3Context()
  const contract_address = "0xC04e1d4Dd28F4be10430318443333767D573eAbC"
  const [packageContract , setPackageContract] = useState("") as any;
  const [tabledata,settableData] = useState('') as any;
  const [createdAts,setcreatedAts] = useState('') as any;
  const [Enddate,setEnddate] = useState('') as any;
  const loadBlockChain=async () =>{

    const web3 = createAlchemyWeb3(
      "wss://eth-rinkeby.ws.alchemyapi.io/v2/c2dVo9Tw5ysL232JeLG2GHStN_xtsNnZ",
    );

    

    // const web3 = new Web3(Web3.givenProvider || 'http://localhost:3000')
    const network = await web3.eth.net.getNetworkType();
    console.log(network,"=======net") // should give you main if you're connected to the main network via metamask...
    // const accounts = await web3.eth.getAccounts()
    // setaccount({account: accounts[0]})
    const tmp_abi:any = ContractAbi;
    const myContract:any = new web3.eth.Contract(tmp_abi, contract_address);
    setPackageContract(myContract);

    const message = myContract.methods.dcbPools("8888").call((err:any, result:any) => {
      if (err){
          console.log("error",err)
      } else {
        settableData(result)
        const date = new Date(result.createDate*1000);
   
        setcreatedAts(date.toLocaleDateString("en-US"))
       var date1 = new Date(result.endDate*1000);
        setEnddate(date1.toLocaleDateString("en-US"))
         console.log("result",result)
      }
  })

  }
  useEffect(() => {
    context.setFirstValidConnector(['MetaMask', 'Infura'])
 
    loadBlockChain()

    
  }, [])
  




  if (!context.active && !context.error) {
    // loading
    return  (<>loading</>)
  } else if (context.error) {
    //error
    return (<>Error</>)
  } else {
    // success
    return (
      
    <>    Your Account is :<Web3Consumer>{(context: { account: boolean | React.ReactChild | React.ReactFragment | React.ReactPortal | null | undefined }) => <p>{context.account}</p>}</Web3Consumer>
    
    <div className="container">
  <h2>Response Table</h2>
           
  <table className="table">
    <thead>
      <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Wallet</th>
        <th>hardcap</th>
        <th>CreatedAt</th>
        <th>EndDate</th>
        <th>Active</th>
      </tr>
    </thead>
    <tbody>
    {tabledata ? (
						<tr >
            <td>{tabledata.agreementID}</td>
            <td>{tabledata.agreementName}</td>
            <td>{tabledata.innovatorWallet}</td>
            <td>{tabledata.hardcap}</td>
            <td>   {createdAts}</td>
            <td>{Enddate}</td>
            {tabledata['active']?( <td>True</td>):( <td>False</td>)}
           </tr>
						):(
              <tr >
              <td>..</td>
              <td>..</td>
              <td>..</td>
              <td>..</td>
              <td>..</td>
              <td>..</td>
                <td>..</td>
             </tr>
						)}
				
      
    </tbody>
  </table>
</div>


   </>
    )
  }
}

