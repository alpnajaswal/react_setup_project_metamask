import  { useEffect,useState} from 'react'
import { userService } from '../_services';

const GetData = () => {
    const [getdata,setGetdata] = useState() as any;
    const [pageNumber,Setpagenumber] = useState(1) as any;


    useEffect(() => { 
     

        userService.Getalldata(pageNumber).then(function (response:any) {
       
            setGetdata(response.data)
          
           }).catch(function(error:any)  {
          });
        
    
        },[]);


    return (<>
        <h4>
     this is a component that retrieves a list from our Restful API: endpoint
/v1/get-all-feedback.
</h4>
<p>I have just created an API call in this page please check inside the file</p>
</>
    )
}

export default GetData