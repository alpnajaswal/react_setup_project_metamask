import {useState} from 'react';

import { Field, reduxForm } from 'redux-form'
import { userService,formValidation } from '../_services';
interface fieldInterface {
    label: any;
	input:any
    placeholder: string;
    name: any;
    value:any;
    type: string;
    onChange:any;
    maxLength:number;
    disabled:false;
	meta: {
		touched: boolean;
		error: string;
	};
}
const renderField = ({label, type = "text",input,meta: { touched, error },maxLength,placeholder,disabled,...otherProps }: fieldInterface) => (
  <div>
    <label>{label}</label>
    <div>
      <input {...input} placeholder={label} type={type} className="input103 w-100"/>
      {touched && error && <span>{error}</span>}
    </div>
  </div>
)
	  const Login = (props:any) => {
	const {handleSubmit,submitting } = props
    
	const [LoginErr,setLoginErr] = useState('');
	const [username,setusername] = useState('');
	
	const submit = (values:any) => {
		console.log(values)
		let params = {
			email:username,
			password: values.password
		   }
		let validate =  formValidation.loginvalidation(values)
		  if (validate) {
			userService.login(params)
			.then(function (response) {
		       if(response.data.data.user.isEmailVerified === false){
			
			  }else{
			   
				localStorage.setItem('value1', 'true');
				localStorage.setItem('accesstoken', response.data.data.access_token);
				localStorage.setItem('userId', response.data.data.user.id);
			  
				
				 setLoginErr("")
			  }
			 
			
			}).catch(error => {
				console.log(error.response.status)
			 if(error.response){
			  console.log(error.response.status)
			  setLoginErr(error.response.data.message)
			}else{
			  setLoginErr("Please Check your server")
			}
			 
		  });	
	   }
	}
	const handleChange= (e:any)=> {
		setusername(e.target.value.toLowerCase())
		
	}
    return (
	
   <section className="loginsec">

   <div className="right_side py-4">
		<div className="container">
			<div className="row">
		        <aside className="col-md-12 m-auto">
					<div className="text-center">
						<h4>Login</h4>
					
					</div>
					<div className="login_form mt-4">
					<span className="error">{LoginErr}</span>
					<form onSubmit={handleSubmit(submit)}>
						<Field
						name="email"
						type="text"
						component={renderField}
						label="Email Address"
                        
						/>
						<Field
						name="password"
						type="password"
						component={renderField}
						label="Password"
						/>
						
							{/* <div className="d-flex justify-content-between">
								<label>
							        <input type="checkbox" checked={true} name="remember"/> Remember me
							     </label>
							     <label><a href="forgot_password.html" className="text-dark">Forgot Password?</a></label>
						    </div> */}
						    <div>
							<button type="submit" disabled={submitting} className="loginbtn btn shadow">LOGIN</button>
						    {/* <a className="loginbtn btn shadow" href={redirecturl}>LOGIN</a> */}
						    </div>
						    <div className="text-center mt-2 mb-4"> 
						    	<p>New to User?   <a href="/signup" className="signup_text">SIGN UP</a></p>
						    </div>
						</form>

						
					</div>
				
				</aside>
			</div>
		</div>
	</div>
</section>

    )
}

export default reduxForm({
	form: 'loginForm' // a unique identifier for this form
  })(Login)