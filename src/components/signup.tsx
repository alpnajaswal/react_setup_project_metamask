
// import DatePicker from "react-datepicker";
// import "react-datepicker/dist/react-datepicker.css";
// import PhoneInput from 'react-phone-number-input'
// import 'react-phone-number-input/style.css'
import { Field, reduxForm } from 'redux-form'
 import { formValidation } from '../_services';
interface fieldInterface {
    label: any;
	input:any
    placeholder: string;
    name: any;
    value:any;
    type: string;
    onChange:any;
    maxLength:number;
    disabled:false;
	meta: {
		touched: boolean;
		error: string;
	};
}
const renderField = ({label, type = "text",input,meta: { touched, error },maxLength,placeholder,disabled,...otherProps }: fieldInterface) => (
  <div>
    <label>{label}</label>
    <div>
      <input {...input} placeholder={label} type={type} className="input103 w-100"/>
      {touched && error && <span>{error}</span>}
    </div>
  </div>
)

const SubmitValidationForm = (props:any) => {
  const {  handleSubmit, submitting } = props

  

	const submit = (values:any) => {
		let params = {
			email:values.email,
			first_name:values.first_name,
			last_name:values.last_name,
			middle_name:values.middle_name,

			
		   }
		   console.log(values)
		    formValidation.profile_validation(params)

		}

  return (




<>

  

<section className="py-4">
	<div className="container-fluid">
		<div className="row">
			<aside className="col-lg-12 text-left">
				<div className="headertitle">
					<h3>Sign Up</h3>
				</div>
			</aside>
		</div>
		<form onSubmit={handleSubmit(submit)}>
		<div className="row mt-4">
		
			<aside className="col-lg-9 text-left">
				<div className="rightside_profile p-3">
					<h5 className="text-dark"></h5>
					{/* <form className="mt-4"> */}
					<div className="row mt-4">
						<div className="col-md-6 mb-3">
						<Field name="first_name" type="text" component={renderField} label="First Name"/>
							
						
						</div>
						<div className="col-md-6 mb-3">
						<Field name="middle_name" type="text" component={renderField} label="Middle Name"/>
						
						</div>
						<div className="col-md-6 mb-3">
						<Field name="last_name" type="text" component={renderField} label="Last Name"/>	
						
						</div>
            <div className="col-md-6 mb-3">
			<Field name="email" type="text" component={renderField} label="Email Address"/>
		
            </div>
					
						<div className="col-md-6 mb-3">
							 <div className="form-group">
							    <label htmlFor="exampleFormControlSelect1">Gender</label>
							    <select className="form-control" id="exampleFormControlSelect1 border-0">
							      <option>Male</option>
							      <option>Female</option>
							      <option>Prefer Not to Disclose</option>
							    </select>
							  </div>
						</div>
					
						<aside className="col-lg-12 my-4 text-right">	<button type="submit" disabled={submitting} className="subbtn1 shadow">SUBMIT</button>
						</aside>
					</div>
					{/* </form> */}
				</div>
			</aside>
			
		</div>
		</form>

	</div>
</section>

</>

  )
}

export default reduxForm({
  form: 'submitValidation'  // a unique identifier for this form
})(SubmitValidationForm)
