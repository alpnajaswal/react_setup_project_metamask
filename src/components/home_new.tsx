import  { useEffect,useState} from 'react'
import { userService } from '../_services';
import { Web3Provider, useWeb3 } from 'react-web3-modal'
import styled from 'styled-components';
const GetData = () => {
    const [getdata,setGetdata] = useState() as any;
    const [pageNumber,Setpagenumber] = useState(1) as any;
    const Button = styled.button`
    position: absolute;
    top: 50vh;
    left: 50vw;
    transform: translate(-50%, -50%);
    width: 160px;
    height: 40px;
    border: 1px solid #4287f5;
    border-radius: 8px;
    background: #4287f5;
    color: #fff;
    cursor: pointer;
    :hover {
      border-color: #2b75ed;
      background: #2b75ed;
    }
    :focus {
      outline: none;
    }
  `;
  
  const ConnectButton = () => {
    const { openWalletModal } = useWeb3();
    return <Button onClick={openWalletModal}>Connect wallet</Button>;
  }
  
  const config = {
    supportedChainIds: [1, 100],
    connectors: {
      walletconnect: {
        rpc: {
          1: `https://mainnet.infura.io/v3/${process.env.REACT_APP_INFURA_ID}`,
          // 100: 'https://dai.poa.network',
        },
        bridge: 'https://bridge.walletconnect.org',
        qrcode: true,
        pollingInterval: 15000
      },
    },
  };
    useEffect(() => { 
     

        userService.Getalldata(pageNumber).then(function (response:any) {
       
            setGetdata(response.data)
          
           }).catch(function(error:any)  {
          });
        
    
        },[]);


    return (<>
        <h4>
     this is a component that retrieves a list from our Restful API: endpoint
/v1/get-all-feedback.
</h4>
<Web3Provider config={config}>
      <ConnectButton />
    </Web3Provider>
</>
    )
}

export default GetData