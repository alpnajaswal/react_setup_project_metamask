// import { authHeader } from '../_helpers';


//  import {config} from '../config/config'
 import {SubmissionError } from 'redux-form'
// import axios from 'axios';
export const formValidation = {
    loginvalidation,
    profile_validation,
  
};

function loginvalidation(params:any) {
  
    let fields = params;
    // let errors :any = [];
    let formIsValid = true;
  
   if (!fields["email"]) {
      formIsValid = false;
      throw new SubmissionError({ email: 'Email address field can’t be empty.', _error: 'Login failed!' })
   
    }
    if (typeof fields["email"] !== "undefined") {
      //regular expression for email validation
      var pattern = new RegExp(/^[_a-z0-9-]+(\.[_a-z0-9-]+)*(\+[a-z0-9-]+)?@[a-z0-9-]+(\.[a-z0-9-]+)*$/i);
      if (!pattern.test(fields["email"])) {
        formIsValid = false;
        
        throw new SubmissionError({ email: 'Entered email address is not valid.', _error: 'Login failed!' })
      }
    }
    
    if (!fields["password"]) {
      formIsValid = false;
      // errors["password"] = "*Please enter your password.";
      throw new SubmissionError({ password: 'Password field can’t be empty.', _error: 'Login failed!' })
    }
  
  
  
    return formIsValid;

}





function profile_validation(params:any) {
  let fields = params;

  let formIsValid = true;

console.log(fields,"===========>fields")
  
  if (!fields["first_name"]) {
    formIsValid = false;
    throw new SubmissionError({ first_name: 'Please enter your first name.', _error: 'Login failed!' })
    // errors["email"] = "*Please enter your email-ID.";
  }
  if (typeof fields["first_name"] !== "undefined") {
    if (fields["first_name"].length > 16) {
       throw new SubmissionError({ first_name: 'Your First Name must be 15 characters or less.', _error: 'Register failed!' })
       
     }else if (fields["first_name"].length < 3 ) {
       throw new SubmissionError({ first_name: 'Your First FName must be atleast 3 characters.', _error: 'Register failed!' })
      // eslint-disable-next-line 
     }else if (fields["first_name"].trim()==="" ) {
       throw new SubmissionError({ first_name: 'Please enter a valid input.', _error: 'Register failed!' })
       
     }
  } 
  // if (!fields["middle_name"]) {
  //   formIsValid = false;
  //   throw new SubmissionError({ middle_name: 'Please enter your first name.', _error: 'Login failed!' })
  // }
  if (typeof fields["middle_name"] !== "undefined") {
    if (fields["middle_name"].length > 16) {
       throw new SubmissionError({ middle_name: 'Your Middle Name must be 15 characters or less.', _error: 'Register failed!' })
       
     }else if (fields["middle_name"].length < 3 ) {
       throw new SubmissionError({ middle_name: 'Your Middle FName must be atleast 3 characters.', _error: 'Register failed!' })
      // eslint-disable-next-line 
     }else if (fields["middle_name"].trim()==="" ) {
       throw new SubmissionError({ middle_name: 'Please enter a valid input.', _error: 'Register failed!' })
       
     }
  } 
  if (!fields["last_name"]) {
    formIsValid = false;
    throw new SubmissionError({ last_name: 'Please enter your last name.', _error: 'Login failed!' })
 
  }
  if (typeof fields["last_name"] !== "undefined") {
    if (fields["last_name"].length > 16) {
       throw new SubmissionError({ last_name: 'Your Last Name must be 15 characters or less.', _error: 'Register failed!' })
       
     }else if (fields["last_name"].length < 3 ) {
       throw new SubmissionError({ last_name: 'Your Last FName must be atleast 3 characters.', _error: 'Register failed!' })
      // eslint-disable-next-line 
     }else if (fields["last_name"].trim()==="" ) {
       throw new SubmissionError({ last_name: 'Please enter a valid input.', _error: 'Register failed!' })
       
     }
  } 
 if (!fields["email"]) {
    formIsValid = false;
    throw new SubmissionError({ email: 'Please enter your email.', _error: 'Login failed!' })
  
  }
  if (typeof fields["email"] !== "undefined") {
    
    var pattern = new RegExp(/^[_a-z0-9-]+(\.[_a-z0-9-]+)*(\+[a-z0-9-]+)?@[a-z0-9-]+(\.[a-z0-9-]+)*$/i);
    if (!pattern.test(fields["email"])) {
      formIsValid = false;
      
      throw new SubmissionError({ email: 'Please enter valid email.', _error: 'Login failed!' })
    }
  }
  if (!fields["phone"]) {
    formIsValid = false;
    throw new SubmissionError({ phone_code: 'Please enter your phone.', _error: 'failed!' })
    // errors["email"] = "*Please enter your email-ID.";
  }
  if (fields["phone"] && fields["phone"].length<4 && fields["phone"]!=="") {
    formIsValid = false;
    throw new SubmissionError({ phone_code: 'Phone number field must be atleast 4 numbers.', _error: 'Login failed!' })
   
  }
  return formIsValid;
}

