import { authHeader } from '../_helpers';
 import {config} from '../config/config'
 
import axios from 'axios';
export const userService = {
    login,
   
    
    register,
    Getalldata,
  
};
const configheaders = {
    headers: authHeader()
    }
  
  function login(params:any) {
      let url =  `${config.apiUserUrl}/users/login`;
      return axios.post(url,params,configheaders)
    }

function register(params:any) {
   let url =  `${config.apiUserUrl}/users`;
    var formData = new FormData();
    formData.append("email", params.email);
    formData.append("firstName", params.firstName);
    formData.append("lastName", params.lastName);
    formData.append("file", params.file);
    formData.append("password", params.password);
    formData.append("phone", params.phone);
    formData.append("countryCode", params.countryCode);
    formData.append("file", params.file);
    formData.append("dob", params.dob);
    const headers = {  headers: {appVersion:0,deviceType:"web", 'Content-Type': 'multipart/form-data'} }
   return axios.post(url,formData,headers)
  }
  function Getalldata(page:any) {
    let url:any =  `${config.apiUserUrl}/get-all-feedback/`+page;
     return axios.get(url,configheaders)
   }